const express = require('express');
const Tasks = require('../models/Tasks');
const auth = require('../middleware/auth');
const router = express.Router();

const createRouter = () => {
    router.post('/', auth, (req, res) => {
        let data = req.body;
        data.user = req.user._id;
        data = new Tasks(req.body);
        console.log(data);
        if (req.user.role === 'admin' || data.user.equals(req.user._id)) {
            data.save()
                .then(result => res.send(result))
                .catch(error => res.status(400).send(error))
        } else {
            res.send({message: 'Вы не можете создавать задачи для других отделов!'});
        }
    });

    router.put('/:id', auth, async (req, res) => {
        let data = req.body;
        data._id = req.params.id;

        data = new Tasks(data);

        const task = await Tasks.findOne({_id: data._id});

        if (req.user.role === 'admin') {
            Tasks.update(task, data)
                .then(result => res.send(result))
                .catch(error => res.status(400).send(error));
        } else if (JSON.stringify(req.user._id) === JSON.stringify(task.user)) {
            Tasks.update(task, data)
                .then(result => res.send(result))
                .catch(error => res.status(400).send(error));
        } else {
            res.send({message: "Вы не можете редактировать чужие задачи!"});
        }
    });

    router.delete('/:id', auth, async (req, res) => {
        const task = await Tasks.findOne({_id: req.params.id});
        if (req.user.role === 'admin') {
            Tasks.deleteOne({_id: req.params.id})
                .then(result => res.send(result))
                .catch(error => res.status(400).send(error));
        } else if (req.user._id === task.user) {
            Tasks.deleteOne({_id: req.params.id})
                .then(result => res.send(result))
                .catch(error => res.status(400).send(error));
        } else {
            res.send({message: "Вы не можете удалять чужие задачи!"});
        }
    });

    router.get('/', auth, (req, res) => {
        if (req.user.role === 'admin') {
            Tasks.find().populate('categoryName').populate('user')
                .then(result => res.send(result))
                .catch(error => res.status(400).send(error));
        } else {
            Tasks.find({user: req.user._id}).populate('categoryName').populate('user')
                .then(result => res.send(result))
                .catch(error => res.status(400).send(error));
        }
    });

    router.get('/:id', auth, (req, res) => {
        Tasks.find({_id: req.params.id}).populate('categoryName').populate('user')
            .then(result => res.send(result[0]))
            .catch(error => res.status(400).send(error));

    });

    return router;
};

module.exports = createRouter;