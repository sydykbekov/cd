const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategoriesSchema = new Schema({
	categoryName: {
        type: String,
        unique: true,
        required: true
    },
    description: String
});

const Categories = mongoose.model('Categories', CategoriesSchema);
module.exports = Categories;