import React from "react";
import { createDrawerNavigator } from "react-navigation";
import TabsNavigation from "./TabsNavigation";
import Sidebar from "../components/Sidebar";
import NewsScreen from "../screens/NewsScreen/NewsScreen";

const RootNavigation = createDrawerNavigator(
	{
		Tabs: {
			screen: TabsNavigation,
			navigationOptions: {
				header: null
			}
		},
		NewsScreen: {
			screen: NewsScreen
		}
	},
	{
		initialRouteName: 'Tabs',
		contentComponent: Sidebar,
		drawerWidth: 300,
		drawerPosition: 'left'
	}
);

export default RootNavigation;