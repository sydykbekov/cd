import React from "react";
import { createStackNavigator } from "react-navigation";
import PostsScreen from "../screens/PostsScreen";
import NotificationsScreen from "../screens/NotificationsScreen";
import AddNewsScreen from "../screens/AddNewsScreen/AddNewsScreen";
import LogoutScreen from "../components/Sidebar/LogoutScreen";

const SidebarNavigation = createStackNavigator({
	Home: {
		screen: PostsScreen
	},
	Saved: {
		screen: PostsScreen
	},
	About: {
		screen: NotificationsScreen
	},
	News: {
		screen: AddNewsScreen
	},
	Logout: {
		screen: LogoutScreen
	}
}, {
	navigationOptions: {
		header: null
	}
});

export default SidebarNavigation;