import React from 'react';
import AuthLoadingScreen from "../components/AuthLoadingScreen";
import LoginScreen from "../screens/LoginScreen/index";
import NewsScreen from "../screens/NewsScreen/NewsScreen";
import { createStackNavigator, DrawerActions } from "react-navigation";
import RootNavigation from "./RootNavigation";
import NavButton from "../components/NavButton";
import { appConfig } from "../config";

export const AppNavigator = createStackNavigator(
	{
		AuthLoading: {
			screen: AuthLoadingScreen,
			navigationOptions: {
				header: null
			}
		},
		Login: {
			screen: LoginScreen,
			navigationOptions: {
				header: null
			}
		},
		Root: {
			screen: RootNavigation,
			navigationOptions: ({navigation}) => ({
				headerTitle: (
					'Countdown'
				),
				headerStyle: {
					backgroundColor: appConfig.appMainColor,
				},
				headerTintColor: '#fff',
				headerTitleStyle: {
					fontWeight: 'bold',
					textAlign: 'center',
					flexGrow: 1,
					fontSize: 26
				},
				headerLeft: (
					<NavButton
						iconName={"md-menu"}
						color="#fff"
						size={40}
						onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}
					/>
				),
				headerRight: (
					<NavButton
						iconName={"ios-alarm"}
						color="#fff"
						size={40}
                        onPress={() => navigation.navigate("NewsScreen")}
					/>
				)
			})
		}
	},
	{
		initialRouteName: 'AuthLoading'
	}
);