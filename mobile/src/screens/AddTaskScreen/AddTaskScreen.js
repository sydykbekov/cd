import React, { Component } from "react";
import {
	View, Text, Dimensions, Picker, ScrollView, TouchableOpacity, CheckBox, ActivityIndicator
} from "react-native";
import { fetchCategories, fetchUsers, createTask } from "../../store/actions/categories";
import { connect } from "react-redux";
import DatePicker from "react-native-datepicker";
import { appConfig } from "../../config";

import styles from '../../components/Input/style';
import Input from "../../components/Input/Input";


class AddTaskScreen extends Component {
	state = {
		user: '',
		title: '',
		description: '',
		date: '',
		time: '',
		categoryName: '',
		priority: false
	};
	
	componentDidMount() {
		this.props.fetchUsers();
		this.props.fetchCategories();
	}
	
	inputChangeHandler = (name, value) => {
		this.setState({[name]: value});
	};
	
	render() {
		return (
			<ScrollView style={{backgroundColor: styles.colors.white}}>
				<View style={styles.container}>
					<View style={styles.header}>
						<Text style={styles.text}>New task</Text>
					</View>
					<View style={styles.content}>
						<View style={styles.pickerContainer}>
							<Picker
								selectedValue={this.state.user}
								style={styles.picker}
								onValueChange={(itemValue) => this.setState({user: itemValue})}>
								<Picker.Item color={appConfig.appMainColor} label="Выберите отдел"/>
								{this.props.users.map(user =>
									<Picker.Item color={appConfig.appMainColor} key={user._id} label={user.username} value={user._id}/>
								)}
							</Picker>
						</View>
						<Input
							placeholder={"Title"}
              style={styles.textInput}
              value={this.state.title}
							onChangeText={value => this.inputChangeHandler('title', value)}
						/>
						<Input
							placeholder={"Description"}
							style={styles.textArea}
							numberOfLines={10}
							multiline={true}
							value={this.state.description}
							onChangeText={value => this.inputChangeHandler('description', value)}
						/>
                        <DatePicker
                            style={styles.datePicker}
                            date={this.state.date}
                            mode="date"
                            placeholder="Выберите дату"
                            format="YYYY-MM-DD"
                            minDate="2018-06-01"
                            maxDate="2040-06-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36,
									borderColor: styles.colors.purple,
                                    borderRadius: 5
                                }
                            }}
                            onDateChange={(date) => {this.setState({date: date})}}
                        />
                        <DatePicker
                            style={styles.datePicker}
                            date={this.state.time}
                            mode="time"
                            placeholder="Выберите время"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36,
                                    borderColor: styles.colors.purple,
                                    borderRadius: 5
                                }
                            }}
                            onDateChange={(time) => {this.setState({time: time})}}
                        />
						<View style={styles.pickerContainer}>
							<Picker
								selectedValue={this.state.categoryName}
								style={styles.picker}
								onValueChange={(itemValue) => this.setState({categoryName: itemValue})}>
								<Picker.Item color="#8d0171" label="Выберите категорию"/>
								{this.props.categories.map(cat =>
									<Picker.Item color="#8d0171" key={cat._id} label={cat.categoryName}
									             value={cat._id}/>
								)}
							</Picker>
						</View>
						<View style={styles.checkbox}>
							<CheckBox
								value={this.state.priority}
								onValueChange={() => this.setState({priority: !this.state.priority})}
							/>
							<Text style={{marginTop: 5, color: styles.colors.purple}}> Приоритет</Text>
						</View>
					</View>
					<TouchableOpacity style={styles.touchable} onPressOut={() => this.props.createTask(this.state)}>
						{this.props.loading ?
							<ActivityIndicator size="small" color="white"/>
							:
							<Text style={styles.text}>Сохранить</Text>}
					</TouchableOpacity>
					<TouchableOpacity style={styles.touchable}>
						<Text style={styles.text}>Обратный отсчет</Text>
					</TouchableOpacity>
				</View>
			</ScrollView>
		)
	}
}


const mapStateToProps = state => ({
	users: state.categories.users,
	categories: state.categories.allCategories,
	loading: state.categories.loading
});

const mapDispatchToProps = dispatch => ({
	fetchUsers: () => dispatch(fetchUsers()),
	fetchCategories: () => dispatch(fetchCategories()),
	createTask: formData => dispatch(createTask(formData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTaskScreen);
