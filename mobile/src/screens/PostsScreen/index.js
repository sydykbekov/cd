import React, { Component } from 'react';
import {
	Text,
	View,
	StyleSheet
} from 'react-native';

import { Agenda } from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';

LocaleConfig.locales['ru'] = {
	monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
	monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
	dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
	dayNamesShort: ['Вс.','Пн.','Вт.','Ср.','Чт.','Пт.','Сб.']
};

LocaleConfig.defaultLocale = 'ru';
export default class PostsScreen extends Component {
	
	state = {
		items: {'2017-05-22': [{name: 'item 1 - any js object'}],
			'2017-05-23': [{name: 'item 2 - any js object'}],
			'2017-05-24': [],
			'2017-05-25': [{name: 'item 3 - any js object'},{name: 'any js object'}],
			'2017-05-28': [{name: 'item 3 - any js object'},{name: 'any js object'}],
			'2017-05-30': [{name: 'item 3 - any js object'},{name: 'any js object'}],
			'2017-06-25': [{name: 'item 3 - any js object'},{name: 'any js object'}],
		}
	};
	
	loadItems = (day) => {
		setTimeout(() => {
			for (let i = -15; i < 5; i++) {
				const time = day.timestamp + i * 24 * 60 * 60 * 1000;
				const strTime = this.timeToString(time);
				if (!this.state.items[strTime]) {
					this.state.items[strTime] = [];
					const numItems = Math.floor(Math.random() * 5);
					for (let j = 0; j < numItems; j++) {
						this.state.items[strTime].push({
							name: 'Item for ' + strTime
						});
					}
				}
			}
			const newItems = {};
			Object.keys(this.state.items).forEach(key => {
				newItems[key] = this.state.items[key];
			});
			this.setState({
				items: newItems
			});
		}, 1000);
	};
	
	renderItem = (item) => {
		return (
			<View style={[styles.item, {height: item.height}]}><Text style={{color: '#000'}}>{item.name}</Text></View>
		);
	};
	
	renderEmptyDate = () => {
		return (
			<View style={styles.emptyDate}><Text>Нет задач!</Text></View>
		);
	};
	
	renderEmptyData = () => {
		return (
			<View style={styles.emptyDate}><Text>На этот день нет задач</Text></View>
		);
	};
	
	rowHasChanged = (r1, r2) => {
		return r1.name !== r2.name;
	};
	
	timeToString = (time) => {
		const date = new Date(time);
		return date.toISOString().split('T')[0];
	};
	
	render() {
		const vacation = {key:'vacation', color: 'red', selectedDotColor: 'blue'};
		const massage = {key:'massage', color: 'blue', selectedDotColor: 'blue'};
		const workout = {key:'workout', color: 'green'};
		return (
			<Agenda
				items={this.state.items}
				//loadItemsForMonth={this.loadItems}
				selected={'2017-05-22'}
				renderItem={this.renderItem}
				renderEmptyDate={this.renderEmptyDate}
				renderEmptyData = {this.renderEmptyData}
				rowHasChanged={this.rowHasChanged}
				markedDates={{
					'2017-05-25': {dots: [vacation, massage, workout], selected: true, selectedColor: 'red'},
					'2017-05-26': {dots: [massage, workout], disabled: true},
				}}
				markingType={'multi-dot'}
				// markingType={'period'}
				// markedDates={{
				// 	'2017-05-08': {textColor: '#666'},
				// 	'2017-05-09': {textColor: '#666'},
				// 	'2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
				// 	'2017-05-21': {startingDay: true, color: 'blue'},
				// 	'2017-05-22': {endingDay: true, color: 'gray'},
				// 	'2017-05-24': {startingDay: true, color: 'gray'},
				// 	'2017-05-25': {color: 'gray'},
				// 	'2017-05-26': {endingDay: true, color: 'gray'}
				// }}
				// monthFormat={'yyyy'}
				//theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
				//renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
			/>
		);
	}
}

const styles = StyleSheet.create({
	item: {
		backgroundColor: 'white',
		flex: 1,
		borderRadius: 5,
		padding: 10,
		marginRight: 10,
		marginTop: 17,
	},
	emptyDate: {
		height: 15,
		flex: 1,
		paddingTop: 30
	}
});