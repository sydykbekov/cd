import React, { Component } from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import {addNewCategory} from "../../store/actions/categories";
import Input from "../../components/Input/Input";
import styles from '../../components/Input/style';
import btnStyle from '../../components/Button/style';

class ProfileScreen extends Component {

	state = {
	  categoryName: '',
		description: ''
	};

    submitCategoryFormHandler = (event) => {
        event.preventDefault();
        this.props.addNewCategory(this.state);
    };

	changeCategoryName = (text) => {
	    this.setState({categoryName: text})
	};

	changeCategoryDescription = (text) => {
	    this.setState({description: text})
    };

	render() {
		return (
			<View style={styles.container}>
          <View style={styles.header}>
              <Text style={styles.text}>Add new category</Text>
          </View>
				<View style={style.content}>
            <Input
                placeholder={"category name"}
                style={styles.textInput}
                value={this.state.categoryName}
                onChangeText={this.changeCategoryName}
            />
            <Input
                style={styles.textArea}
                underlineColorAndroid="transparent"
                placeholder={"category description"}
                placeholderTextColor={"grey"}
                numberOfLines={10}
                multiline={true}
                value={this.state.description}
                onChangeText={this.changeCategoryDescription}
            />
            <TouchableOpacity style={btnStyle.touchable} onPressOut={this.submitCategoryFormHandler}>
                <View style={btnStyle.button}>
                    <Text style={btnStyle.btnText}>Add</Text>
                </View>
            </TouchableOpacity>
				</View>
			</View>
		);
	}
}

const style = StyleSheet.create({
    content: {
        flex: 1,
        width: "100%",
        backgroundColor: "white",
        paddingTop: 50,
        alignItems: "center",
        justifyContent: "flex-start"
    }
});

const mapStateToProps = state => ({
    categories: state.categories.allCategories
});

const mapDispatchToProps = dispatch => ({
    addNewCategory: (categoryData) => dispatch(addNewCategory(categoryData))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);