import React, { Component } from 'react';
import styles from './style';
import { ActivityIndicator, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";

class Button extends Component {
	render() {
		return (
			<TouchableOpacity style={[styles.touchable, this.props.buttonStyle]}
			                  onPressOut={this.props.onPress}>
				<View style={styles.button}>
					{this.props.isSubmitting ? (
						<ActivityIndicator size="small" color="white"/>
					) : (
						<Text style={[styles.btnText, this.props.btnText]}>{this.props.title}</Text>
					)}
				</View>
			</TouchableOpacity>
		)
	}
}

const mapStateToProps = state => ({
	isSubmitting: state.shared.isSubmitting
});

export default connect(mapStateToProps)(Button);