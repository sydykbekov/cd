import React from 'react';
import { StyleSheet, Text, View } from "react-native";
import { appConfig } from "../config";
import moment from 'moment';
import 'moment/locale/ru';

const Task = (item) => {
	return (
		<View style={styles.resourceContainer}>
			<Text>{item.title}</Text>
			<Text>{moment(item.date).format("DD MMM YY")}</Text>
			<Text>{item.time}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	resourceContainer: {
		backgroundColor: appConfig.taskBackgroundColor,
		marginVertical: 10,
		padding: 10,
		borderRadius: 10,
		borderWidth: 2,
		borderColor: appConfig.taskBorderColor,
		
	},
});

export default Task;
