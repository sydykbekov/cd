import React, { Component } from "react";
import { ActivityIndicator, StatusBar, StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import { fetchTasks } from "../../store/actions/tasks";
import { fetchCategories, fetchUsers } from "../../store/actions/categories";
import styles from '../../components/Input/style';


class AuthLoadingScreen extends Component {
	
	componentDidMount() {
		if(this.props.user) {
			this.props.fetchTasks();
			this.props.fetchUsers();
			this.props.fetchCategories();
		}
		this.props.navigation.navigate(this.props.user ? 'Root' : 'Login');
	}
	
	render() {
		return (
			<View style={styles.container}>
				<ActivityIndicator size="large" color="#0000ff"/>
				<StatusBar barStyle="default"/>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user.user
});

const mapDispatchToProps = dispatch => ({
	fetchTasks: () => dispatch(fetchTasks()),
	fetchUsers: () => dispatch(fetchUsers()),
	fetchCategories: () => dispatch(fetchCategories()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen);
