import React, { Component } from 'react';
import {TextInput} from "react-native";
import styles from "./style";

class Input extends Component {
  render() {
    return (
      <TextInput
        placeholder={this.props.placeholder}
        style={this.props.style}
        value={this.props.value}
        autoCapitalize={"none"}
        autoCorrect={false}
        numberOfLines={this.props.numberOfLines}
        multiline={this.props.multiline}
        placeholderTextColor={styles.colors.purple}
        onChangeText={this.props.onChangeText}
        secureTextEntry={this.props.secureTextEntry}
        returnKeyType={this.props.returnKeyType}
        underlineColorAndroid={this.props.underlineColorAndroid}
      />
    )
  }
}


export default Input;