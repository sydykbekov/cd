import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import { persistReducer, persistStore } from "redux-persist";
import storage from 'redux-persist/lib/storage';
import userReducer from "./reducers/user";
import navReducer from "./reducers/navigation";
import sharedReducer from "./reducers/shared";
import categoriesReducer from "./reducers/categories";
import newsesReducer from "./reducers/newses";
import tasksReducer from "./reducers/tasks";
import { navMiddleware } from "../utils/redux";

const middleware = [
	thunkMiddleware,
	navMiddleware
];

const persistConfig = {
	key: "root",
	storage,
	blacklist: ['nav']
};

const rootReducer = combineReducers({
	nav: navReducer,
	user: userReducer,
	shared: sharedReducer,
	categories: categoriesReducer,
	newses: newsesReducer,
	tasks: tasksReducer
});

const pReducer = persistReducer(persistConfig, rootReducer);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

export const store = createStore(pReducer, enhancers);
export const persistor = persistStore(store);