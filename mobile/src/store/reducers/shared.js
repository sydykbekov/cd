import { SUBMITTING_START, SUBMITTING_STOP } from "../actions/actionTypes";

const initialState = {
	isSubmitting: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case SUBMITTING_START:
			return {...state, isSubmitting: true};
		case SUBMITTING_STOP:
			return {...state, isSubmitting: false};
		default:
			return state;
	}
};

export default reducer;