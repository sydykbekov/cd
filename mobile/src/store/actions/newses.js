import axios from '../../axios-api';
import { FETCH_NEWSES_SUCCESS } from "./actionTypes";

const fetchNewsesSuccess = newses => {
	return {type: FETCH_NEWSES_SUCCESS, newses};
};


export const addNews = newsData => {
	return dispatch => {
		axios.post('/newses', newsData);
	}
};

export const fetchNewses = () => {
	return dispatch => {
		return axios.get('/newses').then(
			response => dispatch(fetchNewsesSuccess(response.data))
		)
	};
};