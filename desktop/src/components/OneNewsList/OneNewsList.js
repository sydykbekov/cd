import React from 'react';
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import "./OneNewsList.css";
import config from '../../config';
import notFound from '../../assets/images/not-found.jpeg';
import { FaDownload, FaEdit, FaTrash } from "react-icons/lib/fa/index";
import moment from 'moment';
import {translate} from "../../localization/i18n";

class OneNewsList extends React.Component {

	handleDeleteClick = (e) => {
		e.preventDefault();
		this.props.onDelete(this.props.id);
	};

	
	render() {
		const {id, image, description, file, title, date} = this.props;
		const imageToShow = image ? config.apiUrl + '/uploads/' + image : notFound;
		const linkToDownloadFile = config.apiUrl + '/uploads/' + file;

		return (
			<li className="content_text news">
					<Link className="link_to_one_news" to={'/one_news/' + id}>
						<div className="one_news_list_image_block">
                            <div className="one_news_list_image"
                                 style={{background: `url(${imageToShow}) no-repeat center`, backgroundSize: 'cover'}}/>
                            <p className="one-news_link-title">{title}</p>
						</div>
					</Link>
				<div className="one-news link-description">
					<p>
						{description.length > 20 ? `${description.substr(0, 20)}...` : description}
					</p>
				</div>
				<div className="news_date">
					{moment(date).format('YYYY-MM-DD HH:mm')}
				</div>
				<div className="news_actions">
						<a htmlFor="file" onClick={this.handleDeleteClick}><FaTrash/></a>
						<Link className="edit_news" to={'/news_editor/' + id}>
							<FaEdit/>
						</Link>
					{file &&
						<a href={linkToDownloadFile} title={translate('oneNewsList.linkTitle')} target="blank"><FaDownload/>
						</a>}
				</div>
			</li>
		
		);
	}
}

OneNewsList.propTypes = {
	id: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	description: PropTypes.string.isRequired,
	image: PropTypes.string,
	file: PropTypes.string,
	date: PropTypes.string.isRequired
};


export default OneNewsList;