import React from 'react';
import moment from 'moment';
import './Footer.css';
import { NavLink } from "react-router-dom";

const logo = require("../../assets/images/logo.svg");

const Footer = () => {
	return (
		<footer>
			<div className="container flex-container">
				<div className="left">
					<NavLink exact to="/"><img className="logo-image" src={logo} alt="logo"/></NavLink>
					<p>©{moment().year()}</p>
				</div>
			</div>
		</footer>
	);
};

export default Footer;
