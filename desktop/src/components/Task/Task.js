import React from 'react';
import './Task.css';
import moment from 'moment';
import {Link} from "react-router-dom";

const props = props => {
	return (
		<Link to={`countdown/${props._id}`} className="one-task-container">
            <li key={props._id} className={['one-task', props.priority ? 'one-task--priority' : ''].join(' ')}>
                <div className="one-task_header">
                    <span className='one-task_header_date'>{`${moment(props.date).format('DD.MM.YYYY')} ${props.time}`}</span>
                    <span className='one-task_header_user'>Исполнитель: {(props.user && props.user.username) && props.user.username}</span>
                    <span className='one-task_header_category'>Категория: {props.categoryName.categoryName}</span>
                </div>
                <div className='one-task_title'>{props.title}</div>
                <div className='one-task_description'>{props.description}</div>
            </li>
		</Link>
	);
};

export default props;
