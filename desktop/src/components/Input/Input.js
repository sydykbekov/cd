import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import nanoid from 'nanoid';
import './Input.css';

const id = nanoid();

const Input = ({value, labelText, onChange, type,name, placeholder, required, className, disabled, onClick}) => {
	return (
		<div>
			<Fragment>
				<label htmlFor={id}>{labelText}</label>
				<input
					className={className}
					required={required}
					value={value}
					onChange={onChange}
					id={id}
					name={name}
					placeholder={placeholder}
					type={type}
					disabled={disabled}
					onClick={onClick}
				/>
			</Fragment>
		</div>
	);
};

Input.propTypes = {
	value: PropTypes.any.isRequired,
	labelText: PropTypes.string,
	type: PropTypes.string.isRequired,
	name: PropTypes.string,
	placeholder: PropTypes.string,
	onChange: PropTypes.func,
  required: PropTypes.bool,
	className: PropTypes.string,
  disabled: PropTypes.bool,
	onClick: PropTypes.func
};

export default Input;
