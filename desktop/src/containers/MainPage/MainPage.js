import React, { Component } from 'react';
import { connect } from 'react-redux';
import InfiniteCalendar from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css';
import { fetchTasks } from "../../store/actions/tasks";
import Layout from "../../components/Layout/Layout";
import { translate } from "../../localization/i18n";
import Task from "../../components/Task/Task";
import './MainPage.css';
import moment from 'moment';

class MainPage extends Component {
	componentDidMount() {
		this.props.onFetchTasks();
	}
	
	renderTask = task => {
		return <Task key={task._id} {...task} />
	};
	
	render() {
		console.log(this.props.tasks);
		const maxDate = new Date(new Date().getFullYear() + 2, new Date().getMonth(), new Date().getDate());
		const locale = {
			blank: 'Select a date...',
			headerFormat: 'DD MMMM, dd',
			todayLabel: {
				long: 'Сегодня',
			},
			locale: require('date-fns/locale/ru'),
			weekdays: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
			weekStartsOn: 1,
		};
		
		return (
			<Layout>
				<h2>{translate('mainPage.tasks')}</h2>
				<div className="tasks-container">
					<ul className="tasks-container_list">
						{this.props.tasks.sort(t => t.priority ? -1 : 1).map(this.renderTask)}
					</ul>
					<div className="tasks-container-filter">
						<InfiniteCalendar
							width={300}
							height={300}
							selected={new Date()}
							disabledDays={[]}
							minDate={new Date(2018, 0, 1)}
							min={new Date(2018, 0, 1)}
							max={maxDate}
							locale={locale}
							onSelect={day => alert(moment(day).format('DD MMMM YY'))}
						/>
					</div>
				</div>
			</Layout>
		)
	}
}

const mapStateToProps = state => ({
	user: state.user.user,
	tasks: state.tasks.tasks,
});

const mapDispatchToProps = dispatch => ({
	onFetchTasks: () => dispatch(fetchTasks())
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
