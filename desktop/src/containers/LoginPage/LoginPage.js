import React, { Component } from 'react';
import { connect } from 'react-redux';
import './LoginPage.css';
import { loginUser, setLanguage } from "../../store/actions/users";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import { translate } from '../../localization/i18n';

const rus = require("../../assets/images/rus.png");
const eng = require("../../assets/images/usa.png");

class LoginPage extends Component {
	
	state = {
		username: '',
		password: ''
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		this.props.loginUser(this.state);
	};
	
	render() {
		return (
			<div className="login-page container">
				<div className="change_lang_block login_page">
					<button className={['button_change_lang', this.props.lang === 'RU_ru' ? 'current' : ''].join(' ')}
					        style={{background: `url(${rus}) no-repeat center`, backgroundSize: 'cover'}}
					        onClick={() => this.props.setLanguage('RU_ru')}/>
					<button className={['button_change_lang',this.props.lang === 'EN_en' ? 'current' : ''].join(' ')}
					        style={{background: `url(${eng}) no-repeat center`, backgroundSize: 'cover'}}
					        onClick={() => this.props.setLanguage('EN_en')}/>
				</div>
				<h1 className="login-page_title">Final Countdown</h1>
				<form onSubmit={this.submitFormHandler}>
					<div className="login-page_row row">
						<Input
							value={this.state.username}
							onChange={this.inputChangeHandler}
							name="username"
							type="text"
							labelText={translate('loginPage.login')}
						/>
					</div>
					<div className="login-page_row row">
						<Input
							value={this.state.password}
							onChange={this.inputChangeHandler}
							name="password"
							type="password"
							labelText={translate('loginPage.password')}
						/>
					</div>
					{this.props.loginError &&
					<span className="error">{this.props.loginError.error}</span>
					}
					<div className="row">
						<Button btnClass="login-page_btn">{translate('loginPage.enter')}</Button>
					</div>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	loginError: state.user.loginError,
	lang: state.user.language.language
});

const mapDispatchToProps = dispatch => ({
	loginUser: userData => dispatch(loginUser(userData)),
	setLanguage: lang => dispatch(setLanguage(lang))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);