import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import Layout from "../../components/Layout/Layout";
import {MdAddCircle, MdDelete, MdSave, MdDone, MdCreate} from 'react-icons/lib/md';
import Input from "../../components/Input/Input";
import InputMoment from 'input-moment';
import moment from 'moment';
import './Countdown.css';
import {getTask, saveCountdown} from "../../store/actions/tasks";
import Button from "../../components/Button/Button";
import swal from 'sweetalert2';

class Countdown extends Component {
    state = {
        m: moment(),
        stages: [],
        show: false,
        id: null
    };

    inputChangeHandler = (e, id) => {
        let stages = [...this.state.stages];
        stages[id][e.target.name] = e.target.value;
        this.setState({stages});
    };

    componentDidMount() {
        this.props.getTask(this.props.match.params.id);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id !== this.props.task._id) {
            this.props.getTask(this.props.match.params.id);
        }
        if ((JSON.stringify(this.state.stages) !== JSON.stringify(this.props.task.stages)) && this.state.stages.length === 0) {
            console.log('did update');
            this.setState({stages: this.props.task.stages});
        }
    }

    componentWillUnmount() {
        this.setState({stages: []});
    }

    addStage = () => {
        console.log(this.state.stages);
        let stages = [...this.state.stages];
        let stage = {
            title: '',
            description: '',
            date: '',
            time: '',
            done: false,
            isEditable: true
        };
        stages.push(stage);
        console.log(stages);
        this.setState({stages: stages});
    };

    handleChange = m => {
        this.setState({m});
    };

    removeStage = key => {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true
        });

        swalWithBootstrapButtons({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                swalWithBootstrapButtons(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                );
                let stages = [...this.state.stages];
                stages.splice(key, 1);
                this.setState({stages});
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        });
    };

    datePickerShow = key => {
        this.setState({show: !this.state.show, id: key});
    };

    handleSave = key => {
        let stages = [...this.state.stages];
        stages[key].date = this.state.m.format('DD/MM/YYYY');
        stages[key].time = this.state.m.format('HH:mm');
        this.setState({stages, show: false});
    };

    completeStage = key => {
        let stages = [...this.state.stages];
        stages[key].done = true;
        this.setState({stages});
    };

    save = () => {
        let task = this.props.task;
        task.stages = this.state.stages;
        this.props.saveCountdown(task);
    };

    edit = (e, id) => {
        e.preventDefault();
        let stages = [...this.state.stages];
        stages[id].isEditable = !stages[id].isEditable;
        this.setState({stages});
    };

    confirmComplete = key => {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true
        });

        swalWithBootstrapButtons({
            title: 'Вы уверены что выполнили задание?',
            text: "Отменить будет нельзя!",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Да!',
            cancelButtonText: 'Ой, кое-что забыл)',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                swalWithBootstrapButtons(
                    'Выполнено!',
                    'Вы молодец!',
                    'success'
                );
                this.completeStage(key);
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                    'Отменено',
                    'Скорее доделывайте :)',
                    'info'
                )
            }
        });
    };

    render() {
        const task = this.props.task;
        return (
            <Fragment>
                <Layout>
                    <h3 className="new-task-title">{task.title}</h3>
                    <div className="countdown-container">
                        <div className="datepicker-container" style={{display: this.state.show ? 'block' : 'none'}}
                             onClick={() => this.setState({show: false})}>
                            <div onClick={e => e.stopPropagation()}>
                                <div>
                                    <InputMoment
                                        moment={this.state.m}
                                        onChange={this.handleChange}
                                        minStep={1}
                                        hourStep={1}
                                        onSave={() => this.handleSave(this.state.id)}
                                        prevMonthIcon="ion-ios-arrow-left"
                                        nextMonthIcon="ion-ios-arrow-right"
                                    />
                                </div>
                            </div>
                        </div>
                        {this.state.stages.map((stage, key) =>
                            <form className="stage-container" key={key} onSubmit={(e) => this.edit(e, key)}>
                                <div className="stage-container-body">
                                    <span className="stage-number">Этап {key + 1}</span>
                                    {stage.isEditable ?
                                        <Fragment>
                                            <Input
                                                type="text"
                                                value={stage.title}
                                                name="title"
                                                onChange={(e) => this.inputChangeHandler(e, key)}
                                                required={true}
                                                className="countdown-input"
                                                placeholder="Заголовок"
                                            />
                                            <Input
                                                type="text"
                                                value={stage.description}
                                                name="description"
                                                onChange={(e) => this.inputChangeHandler(e, key)}
                                                required={true}
                                                className="countdown-input"
                                                placeholder="Описание"
                                            />
                                            <Input
                                                type="text"
                                                value={stage.date ? `${stage.date} ${stage.time}` : ''}
                                                onClick={() => this.datePickerShow(key)}
                                                required={true}
                                                placeholder="Выберите дату"
                                                className="countdown-input"
                                                name="date"
                                            />
                                        </Fragment> : <Fragment>
                                            <h3 className="title">{stage.title}</h3>
                                            <p className="description">{stage.description}</p>
                                            <span className="date">{stage.date + ' ' + stage.time}</span>
                                        </Fragment>
                                    }
                                </div>
                                <div className="stage-container-footer">
                                    {!stage.done ? <Fragment>
                                            <button className="stage-footer-icons save" type="submit">{stage.isEditable ?
                                                <MdSave className="footer-icons"/> :
                                                <MdCreate className="footer-icons"/>}</button>
                                            {stage.isEditable &&
                                            <div className="stage-footer-icons delete"
                                                 onClick={() => this.removeStage(key)}><MdDelete
                                                className="footer-icons"/></div>}
                                            {!stage.isEditable &&
                                            <div className="stage-footer-icons done"
                                                 onClick={() => this.confirmComplete(key)}><MdDone
                                                className="footer-icons"/></div>}
                                        </Fragment>
                                        : <div className="task-completed">Задача выполнена</div>}
                                </div>
                            </form>
                        )}
                        <div className="plus-icon-container" onClick={this.addStage}><MdAddCircle
                            className="plus-icon"/></div>
                    </div>
                    <Button onClick={this.save}>SAVE COUNTDOWN</Button>
                </Layout>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    task: state.tasks.task
});

const mapDispatchToProps = dispatch => ({
    saveCountdown: data => dispatch(saveCountdown(data)),
    getTask: id => dispatch(getTask(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Countdown);