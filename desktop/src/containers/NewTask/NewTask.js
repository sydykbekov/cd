import React, { Component } from 'react';
import { connect } from "react-redux";
import Input from "../../components/Input/Input";
import InputMoment from 'input-moment';
import Select from "react-select";
import 'react-select/dist/react-select.css';
import moment from 'moment';
import { createTask } from "../../store/actions/tasks";
import { fetchCategories } from "../../store/actions/categories";
import Button from "../../components/Button/Button";
import Layout from "../../components/Layout/Layout";
import './NewTask.css';
import { fetchUsers } from "../../store/actions/users";
import { translate } from "../../localization/i18n";

class NewTask extends Component {
	state = {
		user: '',
		title: '',
		description: '',
		date: '',
		time: '',
		categoryName: '',
		priority: false,
		m: moment(),
		show: false
	};

	componentDidMount() {
		this.props.fetchUsers();
		this.props.fetchCategories();
	}

	handleChange = m => {
		this.setState({m});
	};

	handleSave = () => {
		const date = this.state.m.format('DD/MM/YYYY');
		const time = this.state.m.format('HH:mm');
		this.setState({date, time, show: false});
	};

	inputChangeHandler = event => {
		this.setState({[event.target.name]: event.target.value});
	};

	selectChangeHandler = (name, user) => {
		if (user) {
			this.setState({[name]: user.value});
		} else {
			this.setState({[name]: ''});
		}
	};

	datePickerShow = (e) => {
		this.setState({show: !this.state.show});
	};

	submitFormHandler = e => {
		e.preventDefault();
		this.props.createTask(this.state, e.target.name);
	};

	changeFormName = value => {
		this.setState({formName: value})
	};

	render() {
		const users = this.props.users ? this.props.users.map(user => {
			return {value: user._id, label: user.username}
		}) : [];

		const categories = this.props.categories ? this.props.categories.map(category => {
			return {value: category._id, label: category.categoryName}
		}) : [];
		console.log(categories)
		return (
			<Layout>
				<h3 className="new-task-title">{translate('createTaskPage.pageTitle')}</h3>
				<form name={this.state.formName && this.state.formName} onSubmit={this.submitFormHandler}
				      className="new-task-form">
					<Select
						name="user"
						value={this.state.user}
						onChange={user => this.selectChangeHandler('user', user)}
						options={users}
						required
						className="add-task-select"
						placeholder={translate('createTaskPage.user')}
					/>
					<Input
						type="text"
						value={this.state.title}
						name="title"
						onChange={this.inputChangeHandler}
						placeholder={translate('createTaskPage.title')}
						required={true}
						className="add-task-input"
					/>
					<Input
						type="text"
						value={this.state.description}
						name="description"
						onChange={this.inputChangeHandler}
						placeholder={translate('createTaskPage.description')}
						required={true}
						className="add-task-input"
					/>
					<Select
						name="categoryName"
						value={this.state.categoryName}
						onChange={user => this.selectChangeHandler('categoryName', user)}
						options={categories}
						required={true}
						className="add-task-select"
						placeholder={translate('createTaskPage.category')}
					/>
					<div className="datePicker" onClick={this.datePickerShow}>
						{this.state.date ? `${this.state.date} ${this.state.time}` : `${translate('createTaskPage.date')}`}
					</div>
					<div className="datepicker-container" style={{display: this.state.show ? 'block' : 'none'}} onClick={this.datePickerShow}>
						<div className="datepicker" onClick={e => e.stopPropagation()}>
              <InputMoment
                moment={this.state.m}
                onChange={this.handleChange}
                minStep={1}
                hourStep={1}
                onSave={this.handleSave}
                prevMonthIcon="ion-ios-arrow-left"
                nextMonthIcon="ion-ios-arrow-right"
              />
						</div>
					</div>
					<label className="priority-label">
						<b>{translate('createTaskPage.priority')}</b>
						<input className="add-task-checkbox" type="checkbox"
						       onClick={() => this.setState({priority: !this.state.priority})}/>
					</label>
					<div className="new-task-btns-container">
						<Button btnClass="add-task-btn" type="submit"
						        onClick={() => this.changeFormName('create')}>{translate('createTaskPage.create')}</Button>
						<Button btnClass="add-task-btn" type="submit"
						        onClick={() => this.changeFormName('countdown')}>{translate('createTaskPage.countDown')}</Button>
					</div>
				</form>
			</Layout>
		)
	}
}

const mapStateToProps = state => ({
	users: state.user.users,
	categories: state.categories.categories,
	loading: state.tasks.loading
});

const mapDispatchToProps = dispatch => ({
	fetchUsers: () => dispatch(fetchUsers()),
	fetchCategories: () => dispatch(fetchCategories()),
	createTask: (formData, name) => dispatch(createTask(formData, name))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewTask);