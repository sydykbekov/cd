import React, { Component } from "react";
import "./AddNewsForm.css"
import { connect } from "react-redux";
import { addNews } from "../../store/actions/newses";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import { FaImage, FaPaperclip } from "react-icons/lib/fa/index";
import Layout from "../../components/Layout/Layout";
import { translate } from "../../localization/i18n";
import notFound from '../../assets/images/not-found.jpeg';
import TextArea from "../../components/TextArea/TextArea";

class AddNewsForm extends Component {
	
	state = {
		title: '',
		description: '',
		image: null,
		file: null,
		preview: null
	};
	
	submitNewsFormHandler = (event) => {
		event.preventDefault();
        const formData = new FormData();
		for (let key in this.state) {
			formData.append(key, this.state[key])
		}
		this.props.addNews(formData);
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	imageChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	fileChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	render() {
		let imageText = translate('addNewsPage.imageText');
		if (this.state.image) {
			imageText = this.state.image.name
		}
		let fileText = translate('addNewsPage.fileText');
		if (this.state.file) {
			fileText = this.state.file.name
		}
		let image = notFound;
		
		return (
			<Layout>
				<div className="container">
					<h1 className="add-news_title">{translate('addNewsPage.title')}</h1>
					<form className="add-news-form" onSubmit={this.submitNewsFormHandler}>
						<div className="add_news_image">
							<img src={this.state.preview || image}
							     style={{width: '50px', height: '50px', border: '1px solid black'}}
							     alt=""/>
						</div>
						<div className="add-user_row row">
							<Input
								value={this.state.title}
								onChange={this.inputChangeHandler}
								name="title"
								type="text"
								labelText={translate('addNewsPage.newsTitle')}
							/>
						</div>
						<div className="add-news_row row">
							<TextArea
								value={this.state.description}
								onChange={this.inputChangeHandler}
								name="description"
								type="text"
								labelText={translate('addNewsPage.newsDescription')}
							/>
						</div>
						<div className="add-news_select add-file">
							<label className="attachFile" htmlFor="file"><FaPaperclip/>
								<span>{fileText}</span>
							</label>
							<input id="file"
							       style={{display: 'none'}}
							       name="file"
							       onChange={this.fileChangeHandler}
							       type="file"
							/>
						</div>
						<div className="add-news_select add-image">
							<label className="addImage" htmlFor="image"><FaImage/><span>{imageText}</span></label>
							<input id="image"
							       style={{display: 'none'}}
							       name="image"
							       onChange={this.imageChangeHandler}
							       type="file"
							/>
						</div>
						<div className="row row-button">
							<Button btnClass="add-news_btn">{translate('addNewsPage.publish')}</Button>
						</div>
					</form>
				</div>
			</Layout>
		);
	}
}

const mapStateToProps = state => ({
	newses: state.newses.news,
});

const mapDispatchToProps = dispatch => ({
	addNews: newsData => dispatch(addNews(newsData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewsForm);