import {
    CHANGE_LANGUAGE,
    FETCH_ONE_USER_SUCCESS,
    FETCH_USERS_FAILURE,
    FETCH_USERS_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS, LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS, USER_EDIT_FAILURE,
} from "../actions/actionType";
import merge from "xtend";

const initialState = {
	registerError: null,
	loginError: null,
	user: null,
	token: null,
	oneUser: null,
	newUserError: null,
	users: [],
	language: {},
    editUserError:null
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case REGISTER_USER_SUCCESS:
			return {...state, registerError: null};
		case REGISTER_USER_FAILURE:
			return {...state, registerError: action.error};
		case LOGIN_USER_SUCCESS:
			return {...state, user: action.user, token: action.token, loginError: null};
		case LOGIN_USER_FAILURE:
			return {...state, loginError: action.error};
		case LOGOUT_USER:
			return {...state, user: null};
		case FETCH_USERS_SUCCESS:
			return merge(state, {users: action.users});
		case FETCH_USERS_FAILURE:
			return {...state, error: action.error};
		case FETCH_ONE_USER_SUCCESS:
			return {...state, oneUser: action.user};
		case CHANGE_LANGUAGE:
			return {...state, language: action.language};
		case USER_EDIT_FAILURE:
			return{...state, editUserError: action.error};
		default:
			return state;
	}
};

export default reducer;