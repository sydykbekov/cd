import { applyMiddleware, compose, createStore } from "redux";
import { routerMiddleware, routerReducer } from "react-router-redux";
import { persistCombineReducers, persistStore } from "redux-persist";
import createHistory from "history/createBrowserHistory";
import thunkMiddleware from "redux-thunk";
import userReducer from "./reducers/users";
import tasksReducer from "./reducers/tasks";
import newsesReducer from "./reducers/newses";
import storage from "redux-persist/lib/storage";
import categoriesReducer from "./reducers/categories";

export const history = createHistory();

const middleware = [
	thunkMiddleware,
	routerMiddleware(history)
];

const persistConfig = {
	key: "root",
	storage,
	blacklist: ['nav']
};

const rootReducer = persistCombineReducers(persistConfig, {
	user: userReducer,
	tasks: tasksReducer,
	newses: newsesReducer,
	categories: categoriesReducer,
	routing: routerReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

export default () => {
	const store = createStore(rootReducer, enhancers);
	const persistor = persistStore(store);
	return {store, persistor};
}