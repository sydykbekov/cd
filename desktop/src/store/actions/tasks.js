import axios from '../../axios-api';
import {
    CREATE_TASK_FAILURE,
    CREATE_TASK_SUCCESS, FETCH_TASK_INFO_SUCCESS,
    FETCH_TASKS_FAILURE,
    FETCH_TASKS_SUCCESS, SAVE_COUNTDOWN_FAILURE, SAVE_COUNTDOWN_SUCCESS,
    START_REQUEST
} from "./actionType";
import { NotificationManager } from 'react-notifications';
import {push} from "react-router-redux";

const startRequest = () => {
	return {type: START_REQUEST};
};

const createTaskSuccess = task => {
	return {type: CREATE_TASK_SUCCESS, task};
};

const createTaskFailure = () => {
	return {type: CREATE_TASK_FAILURE};
};

export const createTask = (formData, name) => {
	return dispatch => {
		dispatch(startRequest());
		axios.post('/tasks', formData).then(response => {
      NotificationManager.success('Успех!', 'Задача успешно создана!');
      dispatch(createTaskSuccess(response.data));
			if (name === 'countdown') {
				dispatch(push(`/countdown/${response.data._id}`));
			}
		}, error => {
			if (error.response.data.name === "ValidationError") {
				NotificationManager.error("Выберите дату!", 'Ошибка!');
			}
			dispatch(createTaskFailure());
		});
	}
};

export const fetchTasks = () => {
	return dispatch => {
		return axios.get('/tasks').then(
			response => dispatch(fetchTasksSuccess(response.data))
		).catch(
			error => dispatch(fetchTasksError(error))
		)
	}
};

const fetchTasksSuccess = tasks => {
	return {type: FETCH_TASKS_SUCCESS, tasks}
};

const fetchTasksError = error => {
	return {type: FETCH_TASKS_FAILURE, error}
};

const saveCountdownSucces = () => {
	return {type: SAVE_COUNTDOWN_SUCCESS};
};

const saveCountdownFailure = () => {
	return {type: SAVE_COUNTDOWN_FAILURE};
};

export const saveCountdown = data => {
	return dispatch => {
		axios.put(`/tasks/${data._id}`, data).then(response => {
			console.log(response.data);
			dispatch(saveCountdownSucces());
      NotificationManager.success('Успех!', 'Изменения сохранены!');
		}, error => {
			console.log(error.response.data);
			dispatch(saveCountdownFailure());
		})
	}
};

const fetchTaskInfoSuccess = task => {
	return {type: FETCH_TASK_INFO_SUCCESS, task};
};

export const getTask = id => {
	return dispatch => {
		axios.get(`/tasks/${id}`).then(response => {
			dispatch(fetchTaskInfoSuccess(response.data));
		}, error => {
			console.log(error.response.data);
		})
	}
};