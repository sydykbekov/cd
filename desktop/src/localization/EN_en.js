export const EN_en = {
	translation: {
		loginPage: {
			login: 'Username',
			password: 'Password',
			enter: 'Log in'
		},
		header: {
			home: 'Home',
			about: 'About',
			calendar: 'Calendar',
			faq: 'FAQ',
			exit: 'Log out'
		},
		mainPage: {
			tasks: 'Tasks'
		},
		newUser: {
			title: 'Admin panel',
			subtitle: 'Add new user',
			username: 'Username',
			password: 'Password',
			description: 'Description',
			role: 'Role',
			register: 'Register',
            userRole: 'User',
			adminRole: 'Administrator',
            imageText: 'Attach an image'
		},
		sidebar: {
			admin: 'Administrator',
			tasks: 'Tasks',
			addUser: 'Add new user',
			addNews: 'Create your news',
			addTask: 'Create task',
			user: 'User',
			allUsers: 'All users',
			news: 'News',
			categories: 'Categories'
		},
		addNewsPage: {
			title: 'Create news',
			newsTitle: 'Title',
			newsDescription: 'Description',
			publish: 'Publish',
            imageText: 'Attach an image',
			fileText: 'Attach a file'
		},
		editNewsPage: {
            title: 'Insert your changes',
            newsTitle: 'Title',
            description: 'Description',
            publish: 'SAVE CHANGES',
            imageText: 'Attach another image',
            fileText: 'Attach another file'
		},
		createTaskPage: {
			pageTitle: 'CREATE TASK',
			title: 'Title',
			description: 'Description',
			date: 'Select a date',
			priority: 'Priority',
			create: 'Create',
			countDown: 'Countdown',
			user: 'Choose user',
			category: 'Choose category'
		},
		categoriesPage: {
			pageTitle: 'Categories',
			createCategory: 'Create category',
			editCategory: 'Edit category',
			categoryName: 'Category name',
			categoryDescription: 'Category description',
			saveButton: 'Save',
			cancelButton: 'Cancel',
			listTitle: 'List of categories'
		},
        allNews: {
			title: 'All news'
		},
        oneNewsList: {
			linkTitle: 'Download file'
		},
        allUsers: {
            pageTitle: 'All users'
		},
        oneUserList: {
            shortDescription: 'Short description: '
		},
		oneUser: {
			title: 'One user window',
			description: 'Description: ',
			role: 'Role: '
		},
		userEditor: {
			title: 'Insert your changes',
			username: 'Username',
			password: 'Password',
			description: 'Description',
			role: 'Role',
			button: 'SAVE CHANGES',
            imageText: 'Attach another image',
			usernameError: 'Username already exists'
		}
	}
};