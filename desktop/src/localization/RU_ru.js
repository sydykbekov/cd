export const RU_ru = {
	translation: {
		loginPage: {
			login: 'Логин',
			password: 'Пароль',
			enter: 'Войти'
		},
		header: {
			home: 'Главная',
			about: 'О программе',
			calendar: 'Календарь',
			faq: 'Вопросы-ответы',
			promo: 'Промокод',
			profile: 'Мой профиль',
			exit: 'Выйти'
		},
		mainPage: {
			tasks: 'Задачи'
		},
		newUser: {
			title: 'Админ панель',
			subtitle: 'Добавить нового пользователя',
			username: 'Логин',
			password: 'Пароль',
			description: 'Описание',
			role: 'Роль',
			register: 'Зарегистрировать',
            userRole: 'Пользователь',
            adminRole: 'Администратор',
            imageText: 'Прикрепить фото'
		},
		sidebar: {
			admin: 'Администратор',
			tasks: 'Задачи',
			news: 'Новости',
			categories: 'Категории',
			addUser: 'Добавить пользователя',
			addNews: 'Создать новость',
			addTask: 'Создать задачу',
			user: 'Пользователь',
            allUsers: 'Все пользователи'
		},
		addNewsPage: {
			title: 'Создать новость',
			newsTitle: 'Заголовок',
			newsDescription: 'Описание',
			publish: 'Опубликовать',
            imageText: 'Прикрепить фото',
            fileText: 'Прикрепить файл'
		},
        editNewsPage: {
            title: 'Внести желаемые изменения',
            newsTitle: 'Заголовок',
            description: 'Описание',
            publish: 'СОХРАНИТЬ ИЗМЕНЕНИЯ',
            imageText: 'Прикрепить другое фото',
            fileText: 'Прикрепить другой файл'
        },
		createTaskPage: {
			pageTitle: 'СОЗДАТЬ ЗАДАЧУ',
			title: 'Заголовок',
			description: 'Описание',
			date: 'Выберите дату',
			priority: 'Приоритет',
			create: 'Создать',
			countDown: 'Обратный отсчет',
			user: 'Выберите пользователя',
			category: 'Выберите категорию'
		},
		categoriesPage: {
			pageTitle: 'Категории',
			createCategory: 'Создать категорию',
			editCategory: 'Редактировать категорию',
			categoryName: 'Название категории',
			categoryDescription: 'Описание категории',
			saveButton: 'Сохранить',
			cancelButton: 'Отменить',
			listTitle: 'Список категории'
		},
        allNews: {
            title: 'Все новости'
        },
        oneNewsList: {
            linkTitle: 'Скачать файл'
        },
		allUsers: {
            pageTitle: 'Все пользователи'
		},
        oneUserList: {
            shortDescription: 'Краткое описание: '
        },
        oneUser: {
            title: 'Окно просмотра пользователя',
            description: 'Описание: ',
            role: 'Роль: '
        },
        userEditor: {
            title: 'Внести желаемые изменения',
            username: 'Имя пользователя',
            password: 'Пароль',
            description: 'Описание',
            role: 'Роль',
            button: 'СОХРАНИТЬ ИЗМЕНЕНИЯ',
            imageText: 'Прикрепить другое фото',
            usernameError: 'Такой пользователь есть'
        }
	}
};