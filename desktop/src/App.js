import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import Routes from "./routes";
import { setLanguage } from "./store/actions/users";

class App extends Component {
	
	componentWillMount() {
		this.props.setLanguage('RU_ru');
	}
	
	render() {
		return (
			<Fragment>
				<NotificationContainer/>
				<Routes user={this.props.user}/>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user.user,
	state: state,
});

const mapDispatchToProps = dispatch => ({
	setLanguage: lang => dispatch(setLanguage(lang))
});

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
